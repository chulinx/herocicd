package main

import (
	"io"
	"net"
	"flag"
	"log"
	"os"
	"fmt"
	"github.com/labstack/gommon/color"
)

var address string

func init() {
	flag.StringVar(&address,"d","","Proxyd Address")
}

func add(a,b int) int {
	return a + b
}


func usage()  {
	fmt.Println(`AppTcpProxy v0.1`)
	flag.PrintDefaults()
}

func main() {
	flag.Parse()
	if address == "" {
		usage()
		os.Exit(1)
	}
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}
	log.Println("Start Server :8080")
	log.Printf("Local address Proxy %s",address)
	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}

		go handleRequest(conn)
	}
}

func handleRequest(conn net.Conn) {
	log.Println("new client")
	proxy, err := net.Dial("tcp", address)
	if err != nil {
		log.Printf(color.Red("ERROR:%s"),err)
	}

	log.Println("proxy connected")
	go copyIO(conn, proxy)
	go copyIO(proxy, conn)
}

func copyIO(src, dest net.Conn) {
	defer src.Close()
	defer dest.Close()
	io.Copy(src, dest)
}