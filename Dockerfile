FROM alpine:latest
RUN mkdir /svr/
COPY AppTcpProxy /svr/
ENV address 127.0.0.1:8080
ENTRYPOINT /svr/AppTcpProxy -d $address